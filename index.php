<?php

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());

use Symfony\Component\HttpFoundation\Request;

// Bootstrap the lowest level of what we need.
require_once DRUPAL_ROOT . '/core/includes/bootstrap.inc';
drupal_bootstrap();

// A request object from the HTTPFoundation. Tell us about the request.
$request = Request::createFromGlobals();
// Run our router, get a response.
$response = router_execute_active_handler($request);
// Output response.
$response->send();
